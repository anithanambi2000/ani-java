package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ArrayListTest {

	public static void main(String[] args) {
		// first type
		Collection c = new ArrayList();
		c.add("ani");
		c.add(123);
		// iterate using iterator
		Iterator<Object> iterator = c.iterator();
		while (iterator.hasNext()) {
			Object s = iterator.next();
			System.out.println("ArrayListTest.main() :: s using iterator--->" + s);
		}

		// second type
		ArrayList al = new ArrayList();
		al.add("ani");
		al.add(123);
		String s1 = (String) al.get(0);
		Integer i = (Integer) al.get(1);
		Object s2 = al.get(0);
		Object i2 = al.get(1);
		System.out.println("ArrayListTest.main() :: s1 --> " + s1);
		System.out.println("ArrayListTest.main() :: i -->" + i);
		System.out.println("ArrayListTest.main() :: s2 -->" + s2);
		System.out.println("ArrayListTest.main() :: i2 -->" + i2);

		// third type
		ArrayList al1 = new ArrayList();
		al1.add("ani");
		al1.add("pavi");
		al1.add("manju");
		// al1.add(123); //no compiler error
		for (int k = 0; k < al1.size(); k++) {
			String s = (String) al1.get(k);
			System.out.println("ArrayListTest.main() :: Arraylist iterartion using for loop :: s --> " + s);
		}

		// fourth type
		ArrayList<String> al2 = new ArrayList();
		al2.add("raj");
		al2.add("anu");
		al2.add("priya");
//		 al2.add(123.0); //compiler throws error
		for (int k = 0; k < al2.size(); k++) {
			String s3 = al2.get(k);
			System.out.println("ArrayListTest.main() :: s3--->" + s3);
		}

		// fifth type
		ArrayList<String> al3 = new ArrayList();
		al3.add("ram");
		al3.add("ragu");
		al3.add("ravi");
		for (String s4 : al3) {
			System.out.println("ArrayListTest.main() :: s4-->" + s4);
		}

		// sixth type
		ArrayList<String> al4 = new ArrayList();
		al4.add("vishnu");
		al4.add("vishal");
		al4.add("vikram");
		Iterator<String> iterator1 = al4.iterator();
		while (iterator1.hasNext()) {
			String s5 = iterator1.next();
			System.out.println("ArrayListTest.main() :: s5-->" + s5);
		}
	}
}

/*
 * 
 * output---->
 * 
 * ArrayListTest.main() :: s using iterator--->ani
ArrayListTest.main() :: s using iterator--->123
ArrayListTest.main() :: s1 --> ani
ArrayListTest.main() :: i -->123
ArrayListTest.main() :: s2 -->ani
ArrayListTest.main() :: i2 -->123
ArrayListTest.main() :: Arraylist iterartion using for loop :: s --> ani
ArrayListTest.main() :: Arraylist iterartion using for loop :: s --> pavi
ArrayListTest.main() :: Arraylist iterartion using for loop :: s --> manju
ArrayListTest.main() :: s3--->raj
ArrayListTest.main() :: s3--->anu
ArrayListTest.main() :: s3--->priya
ArrayListTest.main() :: s4-->ram
ArrayListTest.main() :: s4-->ragu
ArrayListTest.main() :: s4-->ravi
ArrayListTest.main() :: s5-->vishnu
ArrayListTest.main() :: s5-->vishal
ArrayListTest.main() :: s5-->vikram

 * 
 */