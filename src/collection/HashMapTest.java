package collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapTest {

	public static void main(String arg[]) {

		// first type
		HashMap<Integer, String> hmap = new HashMap();
		hmap.put(2, "Anitha");
		hmap.put(3, "Priya");
		hmap.put(6, "Anu");
		hmap.put(8, "Raj");
		Set<Integer> keys = hmap.keySet();
		for (Integer key : keys) {
			System.out.println("HashMapTest.main() :: key-->" + hmap.get(key));
		}

		// second type
		Set<Map.Entry<Integer, String>> entries = hmap.entrySet();
		for (Map.Entry<Integer, String> entry : entries) {
			System.out.println("HashMapTest.main() :: key-->" + entry.getKey());
			System.out.println("HashMapTest.main() :: value-->" + entry.getValue());
		}

		// third type
		Set s = hmap.entrySet();
		Iterator itr = s.iterator();
		while (itr.hasNext()) {
			Map.Entry mentry = (Entry) itr.next();
			System.out.println("HashMapTest.main() :: key -->" + mentry.getKey());
			System.out.println("HashMapTest.main() :: value -->" + mentry.getValue());
		}
	}
}

/*
 * HashMapTest.main() :: key-->Anitha HashMapTest.main() :: key-->Priya
 * HashMapTest.main() :: key-->Anu HashMapTest.main() :: key-->Raj
 * HashMapTest.main() :: key-->2 HashMapTest.main() :: value-->Anitha
 * HashMapTest.main() :: key-->3 HashMapTest.main() :: value-->Priya
 * HashMapTest.main() :: key-->6 HashMapTest.main() :: value-->Anu
 * HashMapTest.main() :: key-->8 HashMapTest.main() :: value-->Raj
 * HashMapTest.main() :: key -->2 HashMapTest.main() :: value -->Anitha
 * HashMapTest.main() :: key -->3 HashMapTest.main() :: value -->Priya
 * HashMapTest.main() :: key -->6 HashMapTest.main() :: value -->Anu
 * HashMapTest.main() :: key -->8 HashMapTest.main() :: value -->Raj
 * 
 */
