package collection;

import java.util.HashSet;
import java.util.Iterator;

public class HashSetTest {
	public static void main(String[] args) {
		HashSet<String> hs = new HashSet();
		boolean b = hs.add("Raj");
		System.out.println("HashSetTest.main() :: b1 -->" + b);
		hs.add("Anitha");
		hs.add("Anu");
		hs.add("Priya");
		b = hs.add("Anitha");
		System.out.println("HashSetTest.main() :: b2 -->" + b);
		System.out.println("HashSetTest.main() :: size -->" + hs.size());
		System.out.println("HashSetTest.main() :: hs -->" + hs);
		Iterator<String> iterator = hs.iterator();
		while (iterator.hasNext()) {
			String s = iterator.next();
			System.out.println("ArrayListTest.main()::s using iterator--->" + s);
		}
		for (String s : hs) {
			System.out.println("HashSetTest.main()::s using forloop---->" + s);
		}
	}
}

/*
 //output
 
HashSetTest.main() :: b1 -->true
HashSetTest.main() :: b2 -->false
HashSetTest.main() :: size -->4
HashSetTest.main() :: hs -->[Priya, Anu, Raj, Anitha]
ArrayListTest.main()::s using iterator--->Priya
ArrayListTest.main()::s using iterator--->Anu
ArrayListTest.main()::s using iterator--->Raj
ArrayListTest.main()::s using iterator--->Anitha
HashSetTest.main()::s using forloop---->Priya
HashSetTest.main()::s using forloop---->Anu
HashSetTest.main()::s using forloop---->Raj
HashSetTest.main()::s using forloop---->Anitha

*/
