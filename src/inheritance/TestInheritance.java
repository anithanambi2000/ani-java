package inheritance;

public class TestInheritance {

	public static void main(String[] args) {
		Dog d=new Dog("Puppy");
		Cat c=new Cat("pussy");
		Horse h=new Horse("swiggy");
		System.out.println("TestInheritance.main()"+d.getName());
		System.out.println("TestInheritance.main()"+c.getName());
		System.out.println("TestInheritance.main()"+h.getName());
		d.bark();
		c.meow();
		h.neigh();

	}

}
