package inheritance;

public class Cat extends AbstractAnimal {

	Cat(String name) {
		super(name);

	}

	public void meow() {
		System.out.println("Cat.meow()");
	}

}
