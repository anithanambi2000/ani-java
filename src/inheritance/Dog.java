package inheritance;

public class Dog extends AbstractAnimal {

	Dog(String name) {
		super(name);

	}

	public void bark() {
		System.out.println("Dog.bark()");
	}
}
