package inheritance;

public abstract class AbstractAnimal implements Animal {
	private String name;

	AbstractAnimal(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
