package classandobject;

public class EmployeeTest {

	public static void main(String[] args) {
		Employee e1 = new Employee();
		System.out.println("EmployeeTest.main() :: e1 --> " + e1);
		e1.setName("Anitha");
		e1.setAge(21);
		e1.setSalary(10000);
		System.out.println("EmployeeTest.main() :: e1 --> " + e1);

		Employee e2 = new Employee("Anitha", 21, 10000);
		System.out.println("EmployeeTest.main() :: e2 --> " + e2);

		System.out.println("EmployeeTest.main() :: e1.equals(e2) --> " + e1.equals(e2));

	}

}
