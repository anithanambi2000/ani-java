package strategy;

public interface Border {
	public void draw();
}
