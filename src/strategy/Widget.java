package strategy;

public class Widget {
	Border border;

	public void setBorder(Border border) {
		this.border = border;
	}

	public void paint() {
		border.draw();
	}
}
