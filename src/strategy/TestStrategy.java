package strategy;

public class TestStrategy {
	public static void main(String arg[]) {
		TextBox tb = new TextBox();
		Button b = new Button();
		RectangularBorder rb = new RectangularBorder();
		RoundedRectangularBorder rrb = new RoundedRectangularBorder();
		tb.setBorder(rb);
		tb.paint();
		tb.setBorder(rrb);
		tb.paint();
		b.setBorder(rb);
		b.paint();
		b.setBorder(rrb);
		b.paint();
	}
}
