package strategy;

public class RectangularBorder implements Border {

	@Override
	public void draw() {
		System.out.println("RectangularBorder.draw()");
	}
}
