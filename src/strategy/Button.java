package strategy;

public class Button extends Widget {
	public void paint() {
		System.out.println("Button.paint()");
		super.paint();
	}
}
