package polymorphism;

public class PolymorphismTest {
	public static void main(String[] args) {
		Shape s = new Shape();
		s.draw();
		Shape s1 = new Rectangle();
		Shape s2 = new Circle();
		Shape s3 = new Square();
		s1.draw();
		s2.draw();
		s3.draw();
	}
}
