package polymorphism;

public class Shape implements IShape {
	@Override
	public void draw() {
		System.out.println("Shape.draw()");
	}
}
