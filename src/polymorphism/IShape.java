package polymorphism;

public interface IShape {
	public void draw();
}
