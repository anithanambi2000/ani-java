package observerpattern;

public class TestObserver {

	public static void main(String[] args) {
		
		CredentialManager cm = new CredentialManager();
		
		TeamManager tmgr = new TeamManager();
		TeamMember tm = new TeamMember();
		TeamLead tl = new TeamLead();
		
		cm.registerILoginLogout(tmgr);
		cm.registerILoginLogout(tm);
		cm.registerILoginLogout(tl);
		
		cm.b = true;
		cm.notifyListener();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		cm.b = false;
		cm.notifyListener();
	}
}
