package observerpattern;

public class TeamMember implements ILoginLogout {

	@Override
	public void login() {
		System.out.println("TeamMember.login()");
	}

	@Override
	public void logout() {
		System.out.println("TeamMember.logout()");
	}
}
