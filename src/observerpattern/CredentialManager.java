package observerpattern;

import java.util.ArrayList;
import java.util.List;

public class CredentialManager implements ICredentialManager {
	private List<ILoginLogout> list;
	public boolean b = true;

	CredentialManager() {
		list = new ArrayList<ILoginLogout>();
	}

	@Override
	public void registerILoginLogout(ILoginLogout i) {
		list.add(i);
	}

	@Override
	public void deregisterILoginLogout(ILoginLogout i) {
		list.remove(i);

	}

	@Override
	public void notifyListener() {

		if (b) {
			for (ILoginLogout i1 : list) {

				i1.login();
			}
		} else {

			for (ILoginLogout i1 : list) {

				i1.logout();
			}

		}
	}
}
