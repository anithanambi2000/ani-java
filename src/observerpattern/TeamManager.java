package observerpattern;

public class TeamManager implements ILoginLogout {

	@Override
	public void login() {
		System.out.println("TeamManager.login()");
	}

	@Override
	public void logout() {
		System.out.println("TeamManager.logout()");
	}
}
