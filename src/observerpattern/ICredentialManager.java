package observerpattern;

public interface ICredentialManager {
	public void registerILoginLogout(ILoginLogout i);

	public void deregisterILoginLogout(ILoginLogout i);

	public void notifyListener();
}
