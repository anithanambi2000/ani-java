package observerpattern;

public interface ILoginLogout {
	public void login();

	public void logout();
}
