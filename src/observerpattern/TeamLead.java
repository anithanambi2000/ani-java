package observerpattern;

public class TeamLead implements ILoginLogout {

	@Override
	public void login() {
		System.out.println("TeamLead.login()");
	}

	@Override
	public void logout() {
		System.out.println("TeamLead.logout()");
	}
}
