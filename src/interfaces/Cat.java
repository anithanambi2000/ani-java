package interfaces;

public class Cat implements Animal {
	private String name;

	Cat(String name) {
		this.name = name;
	}

	@Override
	public String getName() {

		return name;
	}

}
