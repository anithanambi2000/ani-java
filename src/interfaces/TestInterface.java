package interfaces;

public class TestInterface {

	public static void main(String[] args) {
		Dog d = new Dog("puppy");
		Cat c = new Cat("Shinny");
		Horse h = new Horse("John");
		System.out.println("TestInterface.main()--->" + d.getName());
		System.out.println("TestInterface.main()--->" + c.getName());
		System.out.println("TestInterface.main()--->" + h.getName());
	}

}
