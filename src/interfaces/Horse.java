package interfaces;

public class Horse implements Animal {
	private String name;

	Horse(String name) {
		this.name = name;
	}

	@Override
	public String getName() {

		return name;
	}

}
