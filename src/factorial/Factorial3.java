package factorial;

class Factorial31 {
	public int fact(int n) {
		int f = 1;
		for (int i = 1; i <= n; i++) {
			f = f * i;
		}
		return f;
	}
}

public class Factorial3 {

	public static void main(String[] args) {
		Factorial31 f = new Factorial31();
		System.out.println("Factorial3.main()" + f.fact(3));
	}
}
