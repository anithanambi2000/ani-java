package factorial;

final class Factorial21 {
	public static int fact(int n) {

		int f = 1;
		for (int i = 1; i <= n; i++) {
			f = f * i;
		}
		return f;
	}
}

public class Factorial2 {
	public static void main(String[] args) {
		System.out.println("Factorial2.main()--->" + Factorial21.fact(5));
	}
}